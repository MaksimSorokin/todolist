class TodoListComponent {
    todoList = document.createElement('div');
    currentListFilter = 'all'
    todoListHtml = `
        <form id='todoAddForm'>
            <input class='todo-add-form-input' value='new-todo'>
            <button type='submit'>add</button>
        </form>
        <div class='todo-list-container'>
            <h3>todo list</h3>
            <div id='filterButtons'>
                <button data-filter-value='done-only'>show done</button>
                <button data-filter-value='undone-only'>show undone</button>
                <button data-filter-value='all'>show all</button>
            </div>
            <div id='todoList'></div>
        </div>
    `

    constructor() {
        this.todoList.id = 'todoContainer';
        this.todoList.className = 'invisible';
    }

    addTodo = (e) => {
        e.preventDefault();
        const todo = document.querySelector('input.todo-add-form-input').value;
        fetch('https://todo-app-back.herokuapp.com/todos', {
            method: 'POST',
            body: JSON.stringify({
                text: todo,
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem('token')
            }
        }).then(() => {
            this.getTodo(this.currentListFilter);
        })
    }

    getTodo = (filter) => {
        fetch('https://todo-app-back.herokuapp.com/todos', {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem('token'),
            }
        })
            .then(res => res.json())
            .then(res => {
                if (!filter || filter === 'all') return res;
                return res.filter(res => {
                    if (filter === 'done-only') { return res.completed }
                    if (filter === 'undone-only') { return !res.completed }
                })
            })
            .then(res => {
                document.querySelector('#todoList').innerHTML = '';
                res.map(todo => {
                    const todoItem = document.createElement('div');
                    const checkedStatus = todo.completed ? 'checked' : ''
                    const todoItemInnerHtml = `
                    <div class='todo-list-item'>
                        <div class='todo-list-item-date'>${todo.createDate}</div>
                        <input class='todo-list-item-text' value='${todo.text}' data-item-id='${todo._id}'>
                        <input type='checkbox' data-item-id='${todo._id}' ${checkedStatus}>
                        <button class='todo-list-item-remove' data-item-id='${todo._id}'>x</button>
                    </div>
                    `;
                    todoItem.innerHTML = todoItemInnerHtml
                    document.querySelector('#todoList').append(todoItem);
                })
            })
    }

    changeTodoItemStatus = (event) => {
        if (event.target.type !== 'checkbox') return;
        const todoItemId = event.target.getAttribute('data-item-id');
        const updateUrl = `https://todo-app-back.herokuapp.com/todos/${todoItemId}`
        fetch(updateUrl, {
            method: 'PUT',
            body: JSON.stringify({
                completed: event.target.checked,
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem('token'),
            }
        })

    }

    changeTodoItemText = (event) => {
        const enterButtonKeyCode = 13;
        if (event.target.type !== 'text' || event.keyCode !== enterButtonKeyCode) return;
        const todoItemId = event.target.getAttribute('data-item-id');
        const updateUrl = `https://todo-app-back.herokuapp.com/todos/${todoItemId}`
        fetch(updateUrl, {
            method: 'PUT',
            body: JSON.stringify({
                text: event.target.value,
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem('token'),
            }
        })
    }

    deleteTodoItem = (event) => {
        if (event.target.className !== 'todo-list-item-remove') return;
        const todoItemId = event.target.getAttribute('data-item-id');
        const deleteUrl = `https://todo-app-back.herokuapp.com/todos/${todoItemId}`
        fetch(deleteUrl, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem('token'),
            }
        }).then(()=>this.getTodo(this.currentListFilter));
    }

    filter = (event) => {
        const filter = event.target.getAttribute('data-filter-value');
        this.currentListFilter = filter
        this.getTodo(filter)
    }

    export = () => {
        this.todoList.innerHTML = this.todoListHtml;
        this.todoList.querySelector('#todoAddForm').addEventListener('submit', this.addTodo)
        this.todoList.querySelector('#todoList').addEventListener('click', this.changeTodoItemStatus)
        this.todoList.querySelector('#todoList').addEventListener('keyup', this.changeTodoItemText)
        this.todoList.querySelector('#todoList').addEventListener('click', this.deleteTodoItem)
        this.todoList.querySelector('#filterButtons').addEventListener('click', this.filter)


        return this.todoList;
    }
}

class LoginFormComponent {
    loginForm = document.createElement(`form`);
    formHtml = `
    <input class='login' value='smv.gomel@gmail.com'>
    <input class='pass' value='2lvw5zf3pig'>
    <button type='submit'>login</button>
    `
    constructor() {
        this.loginForm.id = 'loginForm';
    }

    login = (event) => {
        event.preventDefault();
        const email = document.querySelector('input.login').value;
        const password = document.querySelector('input.pass').value;

        fetch('https://todo-app-back.herokuapp.com/login', {
            method: 'POST',
            body: JSON.stringify({
                email,
                password,
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(res => res.json())
            .then(res => {
                localStorage.setItem('id', res.id);
                localStorage.setItem('token', res.token);
                todoListComponent.getTodo();
                document.querySelector('form#loginForm').className = 'invisible';
                document.querySelector('#todoContainer').className = '';
            })
    }

    export = () => {
        this.loginForm.addEventListener('submit', this.login)
        this.loginForm.innerHTML = this.formHtml;
        return this.loginForm;
    }
}

const app = document.querySelector('#app');
const loginForm = new LoginFormComponent().export();
app.append(loginForm);
const todoListComponent = new TodoListComponent()
const todoListHtmlElement = todoListComponent.export();
app.append(todoListHtmlElement);